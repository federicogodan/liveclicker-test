var user_id = 0; // saves the user id for get data in edition from

// show info in users tabs
function showUserTab() {
  $('#users').show();
  $('#login').hide();
  $('#edit-user').hide();
  $('#upload-image').hide();
  $('#my-images').hide();
  $('#create-user').hide();

  // reload users
  getUsers();

  // clear create user form
  $('#create-first-name').val('');
  $('#create-last-name').val('');
  $('#create-email').val('');
  $('#create-password').val('');

  // clean edit user form
  $('#edit-first-name').val('');
  $('#edit-last-name').val('');
  $('#edit-email').val('');
  $('#edit-password').val('');
}

// delete the image with provided id
function deleteImage(id) {
  $.ajax({
    type : "DELETE",
    url : "api/file/" + id,
    data : {},
    beforeSend: function (xhr) {
      xhr.setRequestHeader ("Authorization", localStorage.token);
    },
    success : function(res) {
      alert(res.message);
      $('#file-'+id).remove();
    },
    error : function(res) {
      var error = $.parseJSON(res.responseText);
      alert(error.message);
      if (error.message == "Unauthenticated.") {
        showLogin();
      }
    }
  });
}

// delete user with the provided id
function deleteUser(id) {
  $.ajax({
    type : "DELETE",
    url : "api/user/" + id,
    data : {},
    beforeSend: function (xhr) {
      xhr.setRequestHeader ("Authorization", localStorage.token);
    },
    success : function(res) {
      alert(res.message);
      getUsers();
    },
    error : function(res) {
      var error = $.parseJSON(res.responseText);
      alert(error.message);
      if (error.message == "Unauthenticated.") {
        showLogin();
      }
    }
  });
}

// get image with the provided id and load on html
function loadImage(id) {
  $.ajax({
    type : "GET",
    url : "api/file/"+id,
    data : {},
    beforeSend: function (xhr) {
      xhr.setRequestHeader ("Authorization", localStorage.token);
    },
    success : function(res) {
      $('#user-images').append('<li class="list-group-item" id="file-' + id + '"><img src=' + res + ' height="200" width="200"></img><button class="btn btn-danger" onclick="deleteImage(' + id + ')">Delete</button></li>');
    },
    error : function(res) {
      var error = $.parseJSON(res.responseText);
      alert(error.message);
      if (error.message == "Unauthenticated.") {
        showLogin();
      }
    }
  });
}

// get all user images
function reloadUserImages() {
  $('#user-images').empty();
  $.ajax({
    type : "GET",
    url : "api/file",
    data : {},
    beforeSend: function (xhr) {
      xhr.setRequestHeader ("Authorization", localStorage.token);
      xhr.setRequestHeader ("Acept", 'application/json');
    },
    success : function(res) {
      $.each(res.data, function (index, value) {
        loadImage(value.id);
      })

    },
    error : function(res) {
      var error = $.parseJSON(res.responseText);
      alert(error.message);
      if (error.message == "Unauthenticated.") {
        showLogin();
      }
    }
  });
}

// get all users editable for the logged user
function getUsers() {
  $.ajax({
    type : "GET",
    url : "api/user",
    data : {},
    beforeSend: function (xhr) {
      xhr.setRequestHeader ("Authorization", localStorage.token);
      xhr.setRequestHeader ("Acept", 'application/json');
    },
    success : function(res) {
      $('#user-list').empty();
      $.each(res.data, function (index, user) {
        var first_name = user.first_name == null ? '' : user.first_name;
        var last_name = user.last_name == null ? '' : user.last_name;
        var info = '<div class="col-md-6 offset-md-3"><span>First Name: ' + first_name + '</span><br> <span>Last Name: ' + last_name + '</span><br> <span>Email: ' + user.email + '</span></div>';
        $('#user-list').append('<li class="list-group-item" id="user-' + user.id + '"> ' + info +' <div class="col-md-6 offset-md-3"><button class="btn btn-dark" onclick="showEditUserTab(' + user.id + ')">Edit</button> <button class="btn btn-danger" onclick="deleteUser(' + user.id + ')">Delete</button></div></li>');
      })
    },
    error : function(res) {
      var error = $.parseJSON(res.responseText);
      alert(error.message);
      if (error.message == "Unauthenticated.") {
        showLogin();
      }
    }
  });
}

// get user with provided id
function getUser(id) {
  $.ajax({
    type : "GET",
    url : "api/user/" + id,
    data : {},
    beforeSend: function (xhr) {
      xhr.setRequestHeader ("Authorization", localStorage.token);
      xhr.setRequestHeader ("Acept", 'application/json');
    },
    success : function(res) {
      user_id = id;
      $('#edit-first-name').val(res.data.first_name);
      $('#edit-last-name').val(res.data.last_name);
      $('#edit-password').val('');
      $('#edit-email').val(res.data.email);
    },
    error : function(res) {
      var error = $.parseJSON(res.responseText);
      alert(error.message);
      if (error.message == "Unauthenticated.") {
        showLogin();
      }
    }
  });
}

// show tab with edit user form
function showEditUserTab(id) {
  $("#login").hide();
  $('#users').hide();
  $('#login').hide();
  $('#edit-user').show();
  $('#upload-image').hide();
  $('#my-images').hide();
  getUser(id);
}

// hide all and show login form
function showLogin() {
  localStorage.removeItem('expires_at');
  localStorage.removeItem('token');
  $("#dashboard").hide();
  $('#users').hide();
  $('#edit-user').hide();
  $('#upload-image').hide();
  $('#my-images').hide();
  $('#create-user').hide();
  $('#user-list').empty();
  $('#user-images').empty();
  $("#login").show();
}

// show list of images for the user
function showImages() {
  $("#login").hide();
  $("#dashboard").show();
  $('#users').hide();
  $('#login').hide();
  $('#edit-user').hide();
  $('#upload-image').hide();
  $('#my-images').show();
  $('#create-user').hide();
  reloadUserImages();
}

$(document).ready(function() {
  // check if user is logged, if so redirect to dashboard
  if (localStorage.token && Date.parse(localStorage.expires_at) > Date.now()) {
    showImages();
  }

  // call api login to get access token
  // TODO: token is insecured stored in localStorage XSS
  $('#login-form').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
      type : "POST",
      url : "api/login",
      data : {
        email : $('#login-email').val(),
        password: $('#login-password').val(),
      },
      success : function(res) {
        $("#login").hide();
        $("#dashboard").show();
        localStorage.token = "Bearer " + res.access_token;
        localStorage.expires_at = res.expires_at;
        $('#login-email').val('');
        $('#login-password').val('');
        showImages();
      },
      error : function(res) {
        var error = $.parseJSON(res.responseText);
        alert(error.message);
        if (error.message == "Unauthenticated.") {
          showLogin();
        }
      }
    });
  });

  // call api uploading of an image
  $('#upload-image-form' ).submit(function(e) {
    $.ajax({
      url: 'api/file',
      type: 'POST',
      data: new FormData( this ),
      processData: false,
      contentType: false,
      beforeSend: function (xhr) {
        xhr.setRequestHeader ("Authorization", localStorage.token);
      },
      success: function(result) {
        alert(result.message);
        $('#my-images-tab').trigger('click');
      },
      error : function(res) {
        var error = $.parseJSON(res.responseText);
        var message = error.errors.image ? error.errors.image : '';
        alert(error.message + ' ' + message);
        if (error.message == "Unauthenticated.") {
          showLogin();
        }
      }
    });
    e.preventDefault();
  });

  $('#users-tab').on('click', function () {
    showUserTab();
  });

  $('#my-images-tab').on('click', function () {
    showImages();
  });

  $('#upload-image-tab').on('click', function () {
    $('#users').hide();
    $('#login').hide();
    $('#edit-user').hide();
    $('#upload-image').show();
    $('#my-images').hide();
    $('#create-user').hide();
    $('#first_name').val('');
    $('#last_name').val('');
    $('#background').val('');
  });

  $('#create-user-button').on('click', function () {
    $('#users').hide();
    $('#login').hide();
    $('#edit-user').hide();
    $('#upload-image').hide();
    $('#my-images').hide();
    $('#create-user').show();
  });

  $('#reload-user-images').on('click', function () {
    reloadUserImages();
  });

  // call api for edit user
  $('#edit-user-form').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
      type : "PUT",
      url : "api/user/" + user_id,
      data : {
        first_name : $('#edit-first-name').val(),
        last_name : $('#edit-last-name').val(),
        email : $('#edit-email').val(),
        password: $('#edit-password').val(),
      },
      beforeSend: function (xhr) {
        xhr.setRequestHeader ("Authorization", localStorage.token);
        xhr.setRequestHeader ("Acept", 'application/json');
      },
      success : function(res) {
        showUserTab();
      },
      error : function(res) {
        var error = $.parseJSON(res.responseText);
        alert(error.message);
        if (error.message == "Unauthenticated.") {
          showLogin();
        }
      }
    });
  });

  // call api for create user
  $('#create-user-form').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
      type : "POST",
      url : "api/user",
      data : {
        first_name : $('#create-first-name').val(),
        last_name : $('#create-last-name').val(),
        email : $('#create-email').val(),
        password: $('#create-password').val(),
      },
      beforeSend: function (xhr) {
        xhr.setRequestHeader ("Authorization", localStorage.token);
        xhr.setRequestHeader ("Acept", 'application/json');
      },
      success : function(res) {
        $('#users-tab').trigger('click');
      },
      error : function(res) {
        var error = $.parseJSON(res.responseText);
        alert(error.message);
        if (error.message == "Unauthenticated.") {
          showLogin();
        }
      }
    });
  });

  // close session and destroy token
  $('#logout').on('click', function() {
    $.ajax({
      type : "DELETE",
      url : "api/logout",
      data : {},
      beforeSend: function (xhr) {
        xhr.setRequestHeader ("Authorization", localStorage.token);
        xhr.setRequestHeader ("Acept", 'application/json');
      },
      success : function(res) {
        localStorage.removeItem('expires_at');
        localStorage.removeItem('token');
        $("#dashboard").hide();
        $('#users').hide();
        $('#edit-user').hide();
        $('#upload-image').hide();
        $('#my-images').hide();
        $('#create-user').hide();
        $('#user-list').empty();
        $('#user-images').empty();
        $("#login").show();
        alert(res.message);
      },
      error : function(res) {
        var error = $.parseJSON(res.responseText);
        alert(error.message);
        if (error.message == "Unauthenticated.") {
          showLogin();
        }
      }
    });
  });;
});
