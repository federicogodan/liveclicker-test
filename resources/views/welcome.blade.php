<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}"></link>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div id="login">
            <div class="flex-center position-ref full-height">
                <div class="content">
                    <h1>Login</h1>
                    <form id="login-form">
                        <div class="form-group">
                            <label for="login-email">Email address</label>
                            <input type="email" class="form-control" id="login-email" aria-describedby="emailHelp" placeholder="Enter email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="login-password">Password</label>
                            <input type="password" class="form-control" id="login-password" placeholder="Password" name="password">
                        </div>
                        <button type="submit" class="btn btn-success">Login</button>
                    </form>
                    <span>Administrator (user: admin@admin.com, password: secret)</span>
                    <span>Normal User (user: user@user.com, password: secret)
                </div>
            </div>
        </div>

        <div id="dashboard" class="hide">
            <div style="display: inline;">
                <h1>Dashboard</h1>
                <button class="btn btn-danger float-right" id="logout">Logout</button>
            </div>
            <row>
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#my-images" id="my-images-tab">My Images</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#upload-image" id="upload-image-tab">Upload new image</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#users" id="users-tab">Users</a>
                    </li>
                </ul>
            </row>

            <div class="tab-content">
                <!-- List of images -->
                <div id="my-images" class="hide">
                    <p>Info: If you are the admin user you will see all users images in the system, in other case you will see only your user images</p>
                    <button id="reload-user-images" class="btn btn-dark">Reload user images</button>
                    <div class="col-md-6 offset-md-3">
                        <ul id="user-images" class="list-group list-group-flush">
                        </ul>
                    </div>
                </div>

                <!-- List of users -->
                <div id="users" class="hide">
                    <p>Info: If you are the admin user you will see all users in the system, in other case you will see only your user</p>
                    <button id="create-user-button" class="btn btn-success">Create new user</button>
                    <div class="col-md-6 offset-md-3">
                        <ul id="user-list" class="list-group list-group-flush">
                        </ul>
                    </div>
                </div>

                <!-- Edit user form -->
                <div id="edit-user" class="hide">
                    <div class="col-md-6 offset-md-3">
                        <form id="edit-user-form">
                            <div class="form-group">
                                <label for="edit-first-name">First Name</label>
                                <input type="text" class="form-control" id="edit-first-name" name="firstName">
                            </div>
                            <div class="form-group">
                                <label for="edit-last-name">Last Name</label>
                                <input type="text" class="form-control" id="edit-last-name" name="lastName">
                            </div>
                            <div class="form-group">
                                <label for="edit-email">Email address</label>
                                <input type="email" class="form-control" id="edit-email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="edit-password">Password</label>
                                <input type="password" class="form-control" id="edit-password" placeholder="Password" name="password">
                            </div>
                            <button type="submit" class="btn btn-success">Update</button>
                        </form>
                    </div>
                </div>

                <!-- Create user form -->
                <div id="create-user" class="hide">
                    <p>Info: email and password are mandatory</p>
                    <div class="col-md-6 offset-md-3">
                        <form id="create-user-form">
                            <div class="form-group">
                                <label for="create-first-name">First Name</label>
                                <input type="text" class="form-control" id="create-first-name" name="firstName">
                            </div>
                            <div class="form-group">
                                <label for="create-last-name">Last Name</label>
                                <input type="text" class="form-control" id="create-last-name" name="lastName">
                            </div>
                            <div class="form-group">
                                <label for="create-email">Email address</label>
                                <input type="email" class="form-control" id="create-email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="create-password">Password</label>
                                <input type="password" class="form-control" id="create-password" placeholder="Password" name="password">
                            </div>
                            <button type="submit" class="btn btn-success">Create</button>
                        </form>
                    </div>
                </div>

                <!-- Create iamges form -->
                <div id="upload-image" class="hide">
                    <p>Info: if some filed is submited empty, it will be filled with default info</p>
                    <div class="col-md-6 offset-md-3">
                        <form method="POST" action="api/file" enctype="multipart/form-data" id="upload-image-form">
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" id="first_name" class="form-control" placeholder="First Name" name="first_name">
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="form-control" id="last_name" placeholder="Last Name" name="last_name">
                            </div>
                            <div class="form-group">
                                <label for="background">Background</label>
                                <input type="file" class="form-control-file" id="background" placeholder="Background file" name="image">
                            </div>
                            <button id="generate-new-image" type="submit" class="btn btn-success">Generate new image</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
