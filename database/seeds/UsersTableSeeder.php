<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        App\User::create([
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'email' => 'admin@admin.com',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret,
            'is_admin' => true
        ]);
        App\User::create([
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'email' => 'user@user.com',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret,
            'is_admin' => false
        ]);
        factory(App\User::class, 50)->create();
    }
}
