<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\File as FileResource;
use App\Http\Resources\FileCollection;
use App\File;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->isAdmin()) {
            return new FileCollection(File::all());
        }
        return new FileCollection($user->files);
    }

    /**
     * Store a newly created file in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'image' => 'sometimes|bail|max:3000|image'
        ]);

        try {
            if ($request->hasFile('image')) {
                $path = $request->image->store("users/$user->id/images", 'local');
            } else {
                $path =  "users/$user->id/images/".uniqid().".jpeg";
                Storage::disk('local')->copy('default.jpeg', $path);
            }
            $first_name = $request->first_name ? $request->first_name : $user->first_name;
            $last_name = $request->last_name ? $request->last_name : $user->last_name;

            // TODO: add dinamic text size and location
            $image = Image::make(storage_path("app/$path"));
            $image->text("$first_name\n$last_name", $image->width()/2, $image->height()/2, function ($font) {
                $font->file(public_path('fonts/Roboto-Regular.ttf'));
                $font->size(26);
            });
            $image->save();

            $file = File::create([
                'path' => $path,
                'user_id' => $user->id
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Image cant be created, please try again later',
                'data' => new FileResource($file)
            ]);
        }

        return response()->json([
            'message' => 'Image created correctly',
            'data' => new FileResource($file)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(File $file)
    {
        $user = Auth::user();
        if ($user->can('view', $file)) {
            return (string) Image::make(storage_path("app/".$file->path))->encode('data-url');
        }
        return response()->json([
            'message' => 'Your user dont have permissions to perform this action',
        ], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file)
    {
        $user = Auth::user();
        if ($user->can('delete', $file)) {
            try {
                Storage::disk('local')->delete($file->path);
                $file->delete();
                return response()->json([
                    'message' => 'Image deleted successfully',
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'message' => 'This image cant be deleted, please try again later',
                ], 400);
            }
        }
        return response()->json([
            'message' => 'Your user dont have permissions to perform this action',
        ], 401);
    }
}
