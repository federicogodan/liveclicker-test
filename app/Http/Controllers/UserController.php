<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserCollection;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->isAdmin()) {
            return new UserCollection(User::all());
        } else {
            return new UserCollection(collect([$user]));
        }
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'email' => 'bail|required|email|unique:users,email',
            'password' => 'required|string',
        ]);
        if ($user->can('create', User::class)) {
            try {
                $new_user = User::create([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'message' => 'User cant be created, please try again later',
                ], 400);
            }

            return response()->json([
                'message' => 'User created successfully',
                'data' => new UserResource($new_user)
            ]);
        }
        return response()->json([
            'message' => 'Your user dont have permissions to perform this action',
        ], 401);
    }

    /**
     * Display the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $loggued_user = Auth::user();
        if ($loggued_user->can('view', $user)) {
            return new UserResource($user);
        }
        return response()->json([
            'message' => 'Your user dont have permissions to perform this action',
        ], 401);
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'email' => [
                'bail',
                'sometimes',
                'email',
                Rule::unique('users')->ignore($user->id)
            ],
        ]);

        $loggued_user = Auth::user();
        if ($loggued_user->can('update', $user)) {
            try {
                $info = $request->only([
                    'first_name',
                    'last_name',
                    'email',
                    'password'
                ]);
                if ($request->has('password')) {
                    $info['password'] = bcrypt($info['password']);
                }
                $user->fill($info)->save();
            } catch (\Exception $e) {
                return response()->json([
                    'message' => 'User cant be updated, please try again later',
                ], 400);
            }
            return response()->json([
                'message' => 'User updated successfully',
                'data' => new UserResource($user)
            ]);
        }
        return response()->json([
            'message' => 'Your user dont have permissions to perform this action',
        ], 401);
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $loggued_user = Auth::user();
        if ($loggued_user->can('delete', $user)) {
            try {
                $user->files->each(function ($file, $key) {
                    Storage::disk('local')->delete($file->path);
                    $file->delete();
                });
                $user->delete();
            } catch (\Exception $e) {
                return response()->json([
                    'message' => 'User cant be deleted, please try again later',
                ], 400);
            }
            return response()->json([
                'message' => 'User deleted successfully with all their files',
                'data' => new UserResource($user)
            ]);
        };
        return response()->json([
            'message' => 'Your user dont have permissions to perform this action',
        ], 401);
    }
}
