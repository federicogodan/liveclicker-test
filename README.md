# Requirements
You will need to make sure your server meets the following requirements:
- Composer package manager
- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- BCMath PHP Extension
- Imagick PHP Extension

# Installation
Run the following commands in your console:
```sh
$ git clone https://gitlab.com/federicogodan/liveclicker-test.git
$ cd liveclicker-test
$ cp .env.example .env

# edit the .env file database configuration to handle your local database
# dont forget to create the database before start to run the next commands

$ composer install
$ php artisan migrate:fresh
$ php artisan db:seed
$ php artisan passport:install
$ php artisan key:generate
```
# Runing the project
To start the project run:
```sh
$ php artisan serve
```
After that, in your browser, go to http://localhost:8000

# API Mehtods
Login
```sh
POST /api/login HTTP/1.1
Host: localhost:8000
Content-Type: application/json
accept: application/json
cache-control: no-cache
{
	"email" : "admin@admin.com",
	"password" : "secret"
}
```

Logout
```sh
DELETE /api/logout HTTP/1.1
Host: localhost:8000
Content-Type: application/json
accept: application/json
Authorization: Bearer XXXX
```
### User
List users (lists all users if the user is admin)
```sh
GET /api/user HTTP/1.1
Host: localhost:8000
Content-Type: application/json
accept: application/json
```

Show user (get all user info)
```sh
GET /api/user/3 HTTP/1.1
Host: localhost:8000
Content-Type: application/json
accept: application/json
Authorization: Bearer XXXXXXX
```

Update user (admin user can update all users)
```sh
PUT /api/user/3 HTTP/1.1
Host: localhost:8000
Content-Type: application/json
accept: application/json
Authorization: Bearer XXXX
{
	"first_name" : "Pedro",
	"last_name" : "Gonzalez",
	"email" : "pedro@mail.com",
	"password" : "secret2"
}
```

Delete user (admin can delete any user. If user deletes himself he will be loged out from the system. When a user is delete, all they owned images are deleted too)
```sh
DELETE /api/user/1 HTTP/1.1
Host: localhost:8000
Content-Type: application/json
accept: application/json
Authorization: Bearer XXXX
```

### Images
Create image

```sh
POST /api/file HTTP/1.1
Host: localhost:8000
Content-Type: application/x-www-form-urlencoded
Accept: application/json
Authorization: Bearer XXXX
```

Get user images
```sh
GET /api/file HTTP/1.1
Host: localhost:8000
Content-Type: application/json
accept: application/json
Authorization: Bearer XXXX
```

Get image by id (admin user can get any image)
```sh
GET /api/file/11 HTTP/1.1
Host: localhost:8000
Content-Type: application/json
accept: application/json
Authorization: Bearer XXXX
```

Delete image (admin user can delete any image)
```sh
DELETE /api/file/10 HTTP/1.1
Host: localhost:8000
Content-Type: application/json
accept: application/json
Authorization: Bearer XXXX
```

# Considerations
- Frontend dont handle any error, this is done in that way to allow your tests to be permormed with all posible cases like missing fileds, wrong requests, etc.
- Max file size upload is seted to be 3Mb, if that size is exceded an error message will be prompt. If you php.ini file is configured to restrict files under that size (not by default) you must configure that parameters manually.

# Posible improvements
- Frontend must be redesigned :-(
- The api token is stored in browser localStorage, so its vulnerable to XSS atacks if some of the used libraries get compromised in that way. The best way to fix is using a secured cookie with HttpOnly and secure tag enabled.
- Text is always in the middle of the image and with black font. The font size must be set dinamically based on image size so long names dont get out of the image when printed. The font color must be selected by the user, so black images can have text on it too.
- Automated testing is missing :-(. The file liveclicker.postman_collection.json contains all available request for test the API manually, you can import the file as a v2.1 collection in Postman.
